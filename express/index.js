import  express  from "express";
import  bodyParser  from "body-parser";

const PORT = process.env.PORT || 3000;



const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static("assets"));
app.use(express.static("src"));

app.listen(PORT);

console.log('Listening on port: ' + PORT);
